<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Estado extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('estado', function(Blueprint $table)
		{
		    $table->engine = 'InnoDB';
			$table->increments('idestado', true);
			$table->integer('idpais')->unsigned();
			$table->string('nome');
			$table->string('sigla');
			$table->string('regiao')->nullable();
			$table->timestamps();
		});
		
		Schema::table('estado', function(Blueprint $table)
		{
		    $table->engine = 'InnoDB';
		    $table->foreign('idpais')->references('idpais')->on('pais')->onDelete('cascade')->onUpdate('cascade');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('estado');
    }
}
