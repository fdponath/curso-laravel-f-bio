<?php

use Illuminate\Database\Seeder;

class CidadeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $cidades  = array(
            ['idestado'=> '1', 'nome' => 'Cidade RS 1', 'cep' => '90000-000'],
            ['idestado'=> '1', 'nome' => 'Cidade RS 2', 'cep' => '90000-000'],
            ['idestado'=> '2', 'nome' => 'Cidade PR 1', 'cep' => '80000-000'],
            ['idestado'=> '2', 'nome' => 'Cidade PR 2', 'cep' => '80000-000'],
            ['idestado'=> '2', 'nome' => 'Cidade PR 3', 'cep' => '80000-000'],
            );
            DB::Table('cidade')->insert($cidades);
            $this->command->info('Cadastrando cidades');
    }
}
