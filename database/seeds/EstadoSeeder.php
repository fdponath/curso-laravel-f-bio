<?php

use Illuminate\Database\Seeder;

class EstadoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $estados  = array(
            ['idpais'=> '1', 'nome' => 'Rio Grande do Sul', 'sigla' => 'RS', 'regiao' => 'Sul'],
            ['idpais' => '1','nome' => 'Parana', 'sigla' => 'PR', 'regiao' => 'Sul'],
            );
            DB::Table('estado')->insert($estados);
            $this->command->info('Cadastrando estados');
    }
}
