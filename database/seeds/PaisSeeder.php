<?php

use Illuminate\Database\Seeder;

class PaisSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $paises  = array(
            ['nome' => 'Brasil', 'sigla' => 'BRA'],
            ['nome' => 'Alemanha', 'sigla' => 'ALE'],
            ['nome' => 'Canada', 'sigla' => 'CAN'],
            );
            DB::Table('pais')->insert($paises);
            $this->command->info('Cadastrando paises');
    }
}
