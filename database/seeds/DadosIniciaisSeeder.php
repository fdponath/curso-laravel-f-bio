<?php

use Illuminate\Database\Seeder;
use App\Pais;
use App\Estado;
use App\Cidade;

class DadosIniciaisSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
                $this->command->info('Removendo países!');
        $paises = Pais::all();
        foreach ($paises as $pais) {
            $pais->delete();
        }
        $this->command->info('Países removidos!');
        
        $this->command->info('Cadastrando novos países!');
        
        $brasil = new Pais();
        $brasil->nome = 'Brasil';
        $brasil->sigla = 'BRA';
        $brasil->save();
        
        $estadosUnidos = new Pais();
        $estadosUnidos->nome = 'Estados Unidos da América';
        $estadosUnidos->sigla = 'EUA';
        $estadosUnidos->save();
        
        
        $this->command->info('Cadastrando novos estados!');
        
        $rioGrandeDoSul = new Estado();
        $rioGrandeDoSul->idpais = $brasil->idpais;
        $rioGrandeDoSul->nome = 'Rio Grande do Sul';
        $rioGrandeDoSul->sigla = 'RS';
        $rioGrandeDoSul->regiao = 'Sul';
        $rioGrandeDoSul->save();
        
        $santaCatarina = new Estado();
        $santaCatarina->idpais = $brasil->idpais;
        $santaCatarina->nome = 'Santa Catarina';
        $santaCatarina->sigla = 'SC';
        $santaCatarina->regiao = 'Sul';
        $santaCatarina->save();
        
        $maryland = new Estado();
        $maryland->idpais = $estadosUnidos->idpais;
        $maryland->nome = 'Maryland';
        $maryland->sigla = 'MD';
        $maryland->regiao = 'Leste';
        $maryland->save();
        
        $this->command->info('Cadastrando novas cidades!');
        
        $taquara = new Cidade();
        $taquara->idestado = $rioGrandeDoSul->idestado;
        $taquara->nome = 'Taquara';
        $taquara->cep = '95600000';
        $taquara->save();
        
        $igrejinha = new Cidade();
        $igrejinha->idestado = $rioGrandeDoSul->idestado;
        $igrejinha->nome = 'Igrejinha';
        $igrejinha->cep = '95650970';
        $igrejinha->save();
        
        $florianopolis = new Cidade();
        $florianopolis->idestado = $santaCatarina->idestado;
        $florianopolis->nome = 'Florianópolis';
        $florianopolis->save();
        
        $springfield = new Cidade();
        $springfield->idestado = $maryland->idestado;
        $springfield->nome = 'Springfield';
        $springfield->cep = '564215';
        $springfield->save();
        
        $this->command->info('Cidades cadastradas!');

    }
}
