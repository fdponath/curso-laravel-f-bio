<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cidade extends Model
{
    protected $table = 'cidade';

	protected $primaryKey = 'idcidade';
	
	public function estado() {
		return $this->belongsTo('App\Estado', 'idestado', 'idestado', 'estado');
	}
}
