<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Pais;

use App\Http\Requests\PaisRequest;

class PaisController extends Controller
{
    public function exibirPaises() {
        $paises = Pais::all();
        return view('pais.lista', ['paises' => $paises]);
    }
    
    public function exibirCadastro() {
        return view('pais.cadastro');
    }
    
    
    public function cadastrar(PaisRequest $request) {
        $pais = new Pais();
        $pais->nome = Input::get('nome');
        $pais->sigla = Input::get('sigla');
        $pais->save();
        
        return redirect(route('pais-lista'));
        
    }
    
    public function exibirAlteracao($idPais) {
        
        $pais = Pais::findOrFail($idPais);
        
        return view('pais.alteracao', ['pais' => $pais]);
        
    }
    
}

