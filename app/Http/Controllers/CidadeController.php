<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Cidade;

class CidadeController extends Controller
{
 public function exibirCidades1() {
        $listaDeCidades = Cidade::all();
        foreach ($listaDeCidades as $cidade) {
              var_dump($cidade->nome);
              var_dump($cidade->estado->nome);
              var_dump($cidade->estado->pais->nome);
              print '<br>';
        }
 }
 public function exibirCidades2() {
        $cidades = Cidade::all();
        return view('cidade.lista', ['cidades' => $cidades]);
 }
 
 public function exibirCadastro() {
   return view('cidade.cadastro');
 }
 
 public function cadastrar() {
        $cidade = new Cidade();
        $cidade->idestado = 4;
        $cidade->nome = Input::get('nome');
        $cidade->cep = Input::get('cep');
        $cidade->save();
        
        return redirect(route('cidade-lista2'));
        
    }


}
