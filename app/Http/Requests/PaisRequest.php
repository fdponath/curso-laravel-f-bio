<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PaisRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nome' => 'required|unique:pais|max:255',
            'sigla' => 'required|max:3',
        ];
    }
    
        public function messages() {
        return [
            'nome.required' => 'O nome do pais precisa ser preenchido.',
            'sigla.required' => 'A sigla do pais precisa ser preenchida.',
            'sigla.max' => 'A sigla deve possuir no máximo 3 caracteres.',
        ];
        
    }
}
