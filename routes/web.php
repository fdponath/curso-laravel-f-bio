<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------

| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('welcome');
});
Route::get('cidade/lista1', 'CidadeController@exibirCidades1')->name('cidade-lista1');
Route::get('cidade/lista2', 'CidadeController@exibirCidades2')->name('cidade-lista2');
Route::get('cidade/cadastro', 'CidadeController@exibirCadastro')->name('cidade-cadastro');
Route::post('cidade/cadastrar', 'CidadeController@cadastrar')->name('cidade-cadastrar');
Route::get('pais/lista', 'PaisController@exibirPaises')->name('pais-lista');
Route::get('pais/cadastro', 'PaisController@exibircadastro')->name('pais-cadastrar');
Route::post('pais/cadastrar', 'PaisController@cadastrar')->name('pais-cadastrar');
Route::get('pais/alteracao/{idpais}', 'PaisController@exibirAlteracao')->name('pais-alteracao');
