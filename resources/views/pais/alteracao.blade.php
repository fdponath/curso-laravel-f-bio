@extends('layout')

@section('conteudo')

<h1> Cadastro de pais</h1>

<form action="{{route('pais-cadastrar')}}" method="POST">
    {{ csrf_field() }}
    
    <div class="col-md-5 col-sm-3">
    
    <div class="form-group">
    
        <input type="text" name="nome" class="form-control visible-lg" id="nome" placeholder="Nome">
    </div>
      
      <div class="form-group">
        <label for="sigla">Password</label>
        <input type="text" class="form-control" id="sigla" placeholder="Sigla">
      </div>
      
      <a class="btn btn-warning" href="{{  route('pais-lista') }}" role="button">Voltar</a>
      
      <button type="submit" class="btn btn-default">
          <span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span> Cadastrar
      </button>
      
    
    </div>
    
    
</form>



@endsection

