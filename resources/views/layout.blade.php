<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <link href="{{secure_asset('/css/app.css')}}" rel="stylesheet" type="text/css">

    </head>
    <body>
        <div class="flex-center position-ref full-height">
            <div class="content">
                <ul>
                    <!-- comentario feito no branch -->
                    <!-- outro comentario feito no branch -->
                @if (count($errors) > 0)
                    @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                    @endforeach
                @endif
                </ul>
                @yield('conteudo')                
            </div>
        </div>
        <script type="text/javascript" src="{{secure_asset('/js/app.js')}}"></script>
        
    <!--teste de comentários-->
    <!--teste de comentários 2-->
    </body>
</html>